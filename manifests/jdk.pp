class seiaio::jdk {

  package { 'java-1.7.0-openjdk':
    ensure => present
  }
}

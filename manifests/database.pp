class seiaio::database (
  String $root_mysql        = $::seiaio::params::root_mysql,
  String $sei_mysql_pass    = $::seiaio::params::sei_mysql_pass,
  String $sip_mysql_pass    = $::seiaio::params::sip_mysql_pass,
  String $mysql_ipaddr      = $::seiaio::params::mysql_ipaddr,
  String $sigla_organizacao,
  String $nome_organizacao,
  String $dominio,
  ) inherits seiaio::params {

  class { 'mysql::server':
    root_password    => $root_mysql,
    override_options => {
      'mysqld' => {
        'bind_address' => '0.0.0.0',
      }
    }
  }

  mysql::db { 'sei':
    user     => 'user_sei',
    password => $sei_mysql_pass,
    host     => 'localhost',
    charset  => 'latin1',
    collate  => 'latin1_swedish_ci',
    grant    => ['all'],
    notify   => Exec['popula_sei']
  }

  exec { 'popula_sei':
    command     => "mysql -h ${mysql_ipaddr} -u user_sei -p${sei_mysql_pass} sei < ${::seiaio::params::httpd_sei_docroot}/db/sei_2_6_0.sql",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
    notify      => Exec['sei_sigla_organizacao']
  }

  exec { 'sei_sigla_organizacao':
    command     => "mysql -h ${mysql_ipaddr} -u user_sei -p${sei_mysql_pass} sei -e \"update orgao set sigla='${sigla_organizacao}', descricao='${nome_organizacao}' where id_orgao=0;\"",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
    notify      => Exec['sip_seta_pagina_sip']
  }

  mysql::db { 'sip':
    user     => 'user_sip',
    password => $sip_mysql_pass,
    host     => 'localhost',
    charset  => 'latin1',
    collate  => 'latin1_swedish_ci',
    grant    => ['all'],
    notify   => Exec['popula_sip']
  }

  exec { 'popula_sip':
    command     => "mysql -h ${mysql_ipaddr} -u user_sip -p${sip_mysql_pass} sip < ${::seiaio::params::httpd_sei_docroot}/db/sip_2_6_0.sql",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
    notify      => Exec['sip_sigla_organizacao']
  }

  exec { 'sip_sigla_organizacao':
    command     => "mysql -h ${mysql_ipaddr} -u user_sip -p${sip_mysql_pass} sip -e \"update orgao set sigla='${sigla_organizacao}', descricao='${nome_organizacao}' where id_orgao=0;\"",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
    notify      => Exec['sip_seta_pagina_sip']
  }

  exec { 'sip_seta_pagina_sip':
    command     => "mysql -h ${mysql_ipaddr} -u user_sip -p${sip_mysql_pass} sip -e \"update sistema set pagina_inicial='http://${dominio}/sip' where sigla='SIP';\"",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
    notify      => Exec['sip_seta_pagina_sei']
  }

  exec { 'sip_seta_pagina_sei':
    command     => "mysql -h ${mysql_ipaddr} -u user_sip -p${sip_mysql_pass} sip -e \"update sistema set pagina_inicial='http://${dominio}/inicializar.php', web_service='http://${dominio}/controlador_ws.php?servico=sip' where sigla='SEI'\"",
    path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
    refreshonly => true,
  }

}

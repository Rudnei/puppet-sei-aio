class seiaio::fonts {

  package { 'openssl': ensure => present, }
  package { 'libxml2': ensure => present, }
  package { 'curl': ensure => present, }
  package { 'cabextract': ensure => present, }
  package { 'xorg-x11-font-utils': ensure => present, }
  package { 'fontconfig': ensure => present, }
  package { 'wget': ensure => present, }

  exec { '/usr/bin/wget https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm':
    creates => '/tmp/msttcore-fonts-installer-2.6-1.noarch.rpm',
    cwd     => '/tmp',
    require => Package['wget']
  }

  package { 'msttcorefonts':
    ensure   => installed,
    provider => 'rpm',
    source   => '/tmp/msttcore-fonts-installer-2.6-1.noarch.rpm'
  }

}
